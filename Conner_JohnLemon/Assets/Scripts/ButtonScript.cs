﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour

{
    public GameObject wallToDestroy;
    public GameObject floorToDestroy;
    bool pressed = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

private void OnTriggerEnter(Collider other)
{
    if (other.gameObject.CompareTag("Player")&&!pressed)
    {
        pressed = true;
        Destroy(wallToDestroy);
        Destroy(floorToDestroy);
        Destroy(gameObject);
    }
}


}

